package web

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/patrickmn/go-cache"
)

type Web struct {
	Templates  map[string]*template.Template
	ServerPort string
	ServerBind string

	Router chi.Router

	// server cache
	Cache *cache.Cache
}

func New(w *Web) *Web {
	w.Router = chi.NewRouter()

	if w.Templates == nil {
		w.reloadTemplates()
	}

	if w.ServerPort == "" {
		log.Fatal("Invalid port specified to listen on")
	}

	if w.Cache == nil {
		w.Cache = cache.New(30*time.Minute, time.Minute)
	}

	w.Routes()

	return w
}

func (w *Web) reloadTemplates() {
	w.Templates = make(map[string]*template.Template)
	w.templateParse("", "index")
	w.templateParse("index", "")
	w.templateParse("discounts", "")

}

func (w *Web) Start() error {
	log.Printf("Starting server on port %s", w.ServerPort)
	if err := http.ListenAndServe(w.ServerBind+":"+w.ServerPort, w.Router); err != nil {
		return fmt.Errorf("Failed to start http server, %v", err)
	}

	return nil
}

func (w *Web) Routes() {
	// add routes
	w.Router.Route("/", func(r chi.Router) {
		// middlewares to use!
		// r.Use(w.middlewareAuthentication)
		r.Use(w.middlewareExtractTokens())

		// base endpoints
		r.Method("GET", "/", ah(w.HandlerIndex))
		r.Method("POST", "/login", ah(w.HandlerLogin))
		r.Method("GET", "/discounts", ah(w.HandlerDiscounts))
		r.Method("GET", "/activate/{offerid}", ah(w.HandlerActivate))
		// r.Method("GET", "/signup", ah(w.HandlerSignup))

	})
}

func (w *Web) HandlerIndex(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	w.reloadTemplates()
	return w.templateGet(""), nil, http.StatusOK, nil
}
