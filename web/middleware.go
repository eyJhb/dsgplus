package web

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
)

const (
	contextNameTokens = "tokens"
)

func (w *Web) middlewareExtractTokens() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(rw http.ResponseWriter, r *http.Request) {
			// extract cookie userToken if any
			cookieUserTokens, err := r.Cookie(cookieNameTokens)

			// if any error occurs while getting the cookie,
			// we will log if anything besides a no cookie error
			// but will always continue
			if err != nil {
				if err != http.ErrNoCookie {
					log.Printf("middlewareExtractTokens: could no get cookie for some reason, calling next anyways, error: %v", err)
				}

				// either a real error, or a No Cookie Error
				// we will just continue
				next.ServeHTTP(rw, r)
				return
			}

			// get the actual userTokens
			tokensB64 := cookieUserTokens.Value
			tokens, err := base64.StdEncoding.DecodeString(tokensB64)
			if err != nil {
				log.Printf("middlewareExtractTokens: failed to decode the cookie: %v", err)
				next.ServeHTTP(rw, r)
				return
			}

			// unmarshal from a string
			var userTokens []clientCookieToken
			if err := json.Unmarshal(tokens, &userTokens); err != nil {
				log.Printf("middlewareExtractTokens: failed to unmarshal data, continuing anayways: %v", err)
				next.ServeHTTP(rw, r)
				return
			}

			// store tokens in our context!
			r = r.WithContext(context.WithValue(r.Context(), contextNameTokens, userTokens))

			next.ServeHTTP(rw, r)
			return
		}

		return http.HandlerFunc(fn)
	}
}

func tokensFromContext(ctx context.Context) []clientCookieToken {
	// we discard our OK value, as we do not need it,
	// but only so we do not panic! As if we cannot
	// get any value, then we get a nil pointer, and
	// we will use that to determine if we have
	// any information on the user
	tokens, _ := ctx.Value(contextNameTokens).([]clientCookieToken)
	return tokens
}
