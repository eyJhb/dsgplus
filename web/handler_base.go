package web

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/eyJhb/dsg-plus/api"
)

const (
	cookieNameTokens = "token"
)

var (
	ErrNoShopSelected    = errors.New("No shop was selected, and could therefore not login. Please select a shop and try again")
	ErrNoTokenOrEmail    = errors.New("You either have to provide a username/password or a token. Both cannot be empty")
	ErrNotLoggedIn       = errors.New("You are not logged in, please do so before trying to view discounts")
	ErrInvalidLoginOrErr = errors.New("Either the login credentials was invalid, or a unknown error occured, please try again")
)

type clientCookieToken struct {
	Token string `json:"token"`
	Shop  uint8  `json:"shop"`
}

func formGet(values []string) string {
	if len(values) > 0 {
		return values[0]
	}
	return ""
}

func (w *Web) HandlerLogin(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	w.reloadTemplates()
	if err := r.ParseForm(); err != nil {
		return w.templateGet(""), nil, 200, err
	}

	email := formGet(r.PostForm["email"])
	password := formGet(r.PostForm["password"])
	token := formGet(r.PostForm["token"])

	// initialise a tokens map
	var tokens []clientCookieToken

	// we use the token over a username/password
	if token == "" {
		if email == "" || password == "" {
			return w.templateGet(""), nil, 200, ErrNoTokenOrEmail
		}

		// get our shops!
		shops := r.PostForm["shops"]
		if len(shops) == 0 {
			return w.templateGet(""), nil, http.StatusOK, ErrNoShopSelected
		}

		// loop over shops, and try to login with the credentials
		for _, shop := range shops {
			// convert shop string to int
			shopInt, err := strconv.Atoi(shop)
			if err != nil {
				return w.templateGet(""), nil, http.StatusOK, err
			}
			realShop := api.Shop(shopInt)

			// init api client, and then try to login
			a, err := api.New(&api.API{
				Email:    &email,
				Password: &password,
				Shop:     &realShop,
			})
			if err != nil {
				return w.templateGet(""), nil, http.StatusOK, err
			}

			if err := a.Login(r.Context()); err != nil {
				log.Printf("HandlerLogin could not login with error: %v", err)
				return w.templateGet(""), nil, http.StatusOK, ErrInvalidLoginOrErr
			}

			tokens = append(tokens, clientCookieToken{
				Token: *a.Token,
				Shop:  uint8(shopInt),
			})
		}
	} else {
		// validate the given token
		// init api client, and then try to check
		a, err := api.New(&api.API{
			Token: &token,
		})
		if err != nil {
			return w.templateGet(""), nil, http.StatusOK, err
		}

		res, err := a.Check(r.Context())
		if err != nil {
			return w.templateGet(""), nil, http.StatusOK, err
		}

		tokens = append(tokens, clientCookieToken{
			Token: token,
			Shop:  uint8(res.TenantID),
		})
	}

	// marshal tokens into json
	tokensBytes, err := json.Marshal(tokens)
	if err != nil {
		return w.templateGet(""), nil, http.StatusOK, err
	}
	tokensBytesB64 := base64.StdEncoding.EncodeToString(tokensBytes)

	cookie := http.Cookie{
		Name:  cookieNameTokens,
		Value: tokensBytesB64,

		HttpOnly: true,
		Expires:  time.Now().Add(5 * 365 * 24 * time.Hour),
	}
	http.SetCookie(rw, &cookie)

	http.Redirect(rw, r, "/discounts", 303)
	return w.templateGet(""), nil, 0, nil
}

func (w *Web) HandlerDiscounts(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	w.reloadTemplates()

	// set our default template
	dtmpl := w.templateGet("discounts")

	// get the tokens that we have available
	tokens := tokensFromContext(r.Context())
	if tokens == nil {
		return w.templateGet(""), nil, http.StatusOK, ErrNotLoggedIn
	}

	type tmplDataStructInner struct {
		ShopId             uint8
		MembershipOffers   []api.APIResMembershipOffer
		PersonalizedOffers []api.APIResPersonalizedOffer
		PunchCards         []api.APIResPunchCard
	}

	tmplData := make(map[string]tmplDataStructInner)

	// loop over our tokens, and try to get discounts
	for _, clientToken := range tokens {
		token := clientToken.Token

		a, err := api.New(&api.API{
			Token: &token,
		})
		if err != nil {
			return dtmpl, nil, http.StatusOK, err
		}

		// inner data
		tokenData := tmplDataStructInner{ShopId: clientToken.Shop}

		// check our cache if we have anything!
		tokenHashed := hashSha256(token)
		cacheMemberKey := "member-" + tokenHashed
		cachePersonKey := "personal-" + tokenHashed
		if cacheMemOffers, found := w.Cache.Get(cacheMemberKey); found {
			tokenData.MembershipOffers = cacheMemOffers.([]api.APIResMembershipOffer)
		} else {
			// get membershipOffers
			memOffers, err := a.MembershipOffers(r.Context())
			if err != nil {
				return dtmpl, nil, http.StatusOK, err
			}
			tokenData.MembershipOffers = memOffers
			w.Cache.SetDefault(cacheMemberKey, memOffers)
		}
		if cachePersonOffers, found := w.Cache.Get(cachePersonKey); found {
			tokenData.PersonalizedOffers = cachePersonOffers.([]api.APIResPersonalizedOffer)
		} else {
			// get personalizedOffers
			personOffers, err := a.PersonalizedOffers(r.Context())
			if err != nil {
				return dtmpl, nil, http.StatusOK, err
			}
			tokenData.PersonalizedOffers = personOffers
			w.Cache.SetDefault(cachePersonKey, personOffers)
		}

		tmplData[api.ShopToString[api.Shop(clientToken.Shop)]] = tokenData
	}

	return dtmpl, tmplData, http.StatusOK, nil
}

func (w *Web) HandlerActivate(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	dtmpl := w.templateGet("discounts")

	w.reloadTemplates()
	// parse the form
	if err := r.ParseForm(); err != nil {
		return dtmpl, nil, 200, err
	}

	// get the offer id
	offerId := chi.URLParam(r, "offerid")

	// get the shop id
	if len(r.Form["shop"]) == 0 {
		return dtmpl, nil, 200, errors.New("No shop id was provided")
	}
	shop, err := strconv.Atoi(r.Form["shop"][0])
	if err != nil {
		return dtmpl, nil, 200, err
	}

	// get the tokens that we have available
	tokens := tokensFromContext(r.Context())
	if tokens == nil {
		return w.templateGet(""), nil, http.StatusOK, ErrNotLoggedIn
	}

	// loop over our tokens, and try to get discounts
	var ct *clientCookieToken
	for i, clientToken := range tokens {
		if clientToken.Shop != uint8(shop) {
			continue
		}

		ct = &tokens[i]
		break
	}

	if ct == nil {
		return dtmpl, nil, 200, errors.New("could not find a api key for the shop that the offer should be activated for")
	}

	// init api
	a, err := api.New(&api.API{
		Token: &ct.Token,
	})
	if err != nil {
		return dtmpl, nil, http.StatusOK, err
	}

	if err := a.PersonalizedOfferActivate(r.Context(), offerId); err != nil {
		return dtmpl, nil, http.StatusOK, err
	}

	http.Redirect(rw, r, "/discounts", 303)
	return w.templateGet(""), nil, 0, nil
}
