package web

import (
	"html/template"
	"log"
	"net/http"
	"strings"
)

const (
	templatesBasePath = "web/templates/"
	templatesExt      = ".tmpl"
)

var funcMap = template.FuncMap{
	"changeImage": func(image string) string {
		// only change THUMB_LARGE to THUMB_MEDIUM
		return strings.Replace(image, "THUMB_LARGE", "THUMB_MEDIUM", -1)
	},
}

func (w *Web) templateParse(name, path string) {
	// default path to name
	if path == "" {
		path = name
	}

	if _, ok := w.Templates[name]; ok {
		log.Panicf("Trying to ready a template which has already been prepared")
		return
	}

	w.Templates[name] = template.Must(template.New("").Funcs(funcMap).ParseFiles(templatesBasePath+path+templatesExt, templatesBasePath+"base"+templatesExt))
}

func (w *Web) templateGet(name string) *template.Template {
	// 404
	if _, ok := w.Templates[name]; !ok {
		log.Printf("Trying to get a template that does not exists, returning a 404 page, template: %s", name)
		return w.Templates["404.tmpl"]
	}

	// return tmpl
	return w.Templates[name]
}

func (w *Web) templateExec(rw http.ResponseWriter, name string, data interface{}) error {
	if err := w.templateGet(name).ExecuteTemplate(rw, "base", data); err != nil {
		log.Printf("Failed to view template '%s', with data %+v, got error: %v", name, data, err)
		return err
	}

	return nil
}
