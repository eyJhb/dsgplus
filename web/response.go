package web

import (
	"html/template"
	"log"
	"net/http"
)

type ah func(http.ResponseWriter, *http.Request) (template *template.Template, data interface{}, statusCode int, err error)

func (fn ah) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// template data structure, for our requests
	type tmplDataStructure struct {
		Error error
		// User  *models.User
		Data interface{}
	}

	// execute the handler
	tmpl, data, statusCode, err := fn(w, r)

	// 500 means internal error, which we do not pass on
	if statusCode == 500 {
		log.Printf("ServeHTTP: Internal error happened with error: %v", err)
		err = nil
	}

	// if we do not have a 0 error, write the statusCode
	if statusCode != 0 {
		w.WriteHeader(statusCode)
	}

	// if no template set, then do nothing
	if tmpl == nil {
		log.Print("ServeHTTP: called with nil template")
		return
	}

	// setup our data to the template
	tmplData := tmplDataStructure{
		Data:  data,
		Error: err,
	}

	// try to get any user data from our context
	// user := UserFromContext(r.Context())
	// if user != nil {
	// 	tmplData.User = user
	// }

	// if err := w.templateGet(name).ExecuteTemplate(rw, "base", data); err != nil {
	if err := tmpl.ExecuteTemplate(w, "base", tmplData); err != nil {
		log.Printf("ServeHTTP: failed to execute template '%s', got error: %v", tmpl.Name(), err)
	}
}
