package api

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type APIResError struct {
	ErrorName      string      `json:"errorName"`
	Message        string      `json:"message"`
	ErrorTraceID   string      `json:"errorTraceId"`
	RequestTraceID string      `json:"requestTraceId"`
	ClientTraceID  string      `json:"clientTraceId"`
	Data           interface{} `json:"data"`
}

func (a *API) httpGET(ctx context.Context, url string) ([]byte, error) {
	return a.httpRequest(ctx, "GET", url, nil)
}

func (a *API) httpPOST(ctx context.Context, url string, body []byte) ([]byte, error) {
	return a.httpRequest(ctx, "POST", url, bytes.NewReader(body))
}

// httpRequest makes a request using the HTTP client and the obtained token
// if no token is given/obtained using login, then it will throw a error.
// This function only adds the authorization header, and then sends the request
func (a *API) httpRequest(ctx context.Context, method, url string, body io.Reader) ([]byte, error) {
	// call this before anything else
	if err := a.ensureInitialised(); err != nil {
		return nil, err
	}

	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("authorization", "Bearer "+*a.Token)

	res, err := a.client.Do(req)
	if err != nil {
		return nil, err
	}

	byteBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if strings.Contains(string(byteBody), "errorName") {
		// parse the error
		var apiErr APIResError
		if err := json.Unmarshal(byteBody, &apiErr); err != nil {
			return nil, fmt.Errorf("httpRequest.apiError: Got error trying to unwrap request %w", err)
		}

		return nil, fmt.Errorf("httpRequest.apiError: %s: %s", apiErr.ErrorName, apiErr.Message)
	}

	return byteBody, nil
}

func getBody(res *http.Response) []byte {
	body, _ := ioutil.ReadAll(res.Body)
	return body
}

func printBody(res *http.Response) {
	fmt.Println(getBody(res))
}

// simple crypto nounce, does not
// need to be secure, just generate
// something "random"
func mustNounce(length int) string {
	nounce, err := nounce(length)
	if err != nil {
		log.Panicf("mustNounce: failed to get nounce: %v", err)
	}

	return nounce
}

func nounce(length int) (string, error) {
	output := make([]byte, length)
	if _, err := io.ReadFull(rand.Reader, output); err != nil {
		return "", err
	}

	return hex.EncodeToString(output), nil
}
