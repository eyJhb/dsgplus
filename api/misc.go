package api

import (
	"context"
	"encoding/json"
	"time"
)

type Card struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Created        string `json:"created"`
	CardNumber     string `json:"cardNumber"`
	CardTypeID     int    `json:"cardTypeId"`
	ExpiryYear     int    `json:"expiryYear"`
	Blacklisted    bool   `json:"blacklisted"`
	ExpiryMonth    int    `json:"expiryMonth"`
	CardHolderName string `json:"cardHolderName"`
}

type APIResCheck struct {
	ID            string      `json:"id"`
	TenantID      int         `json:"tenantId"`
	MemberID      string      `json:"memberId"`
	Cards         []Card      `json:"cards"`
	CreatedAt     time.Time   `json:"createdAt"`
	UpdatedAt     time.Time   `json:"updatedAt"`
	DiscountTotal interface{} `json:"discountTotal"`
	CardExpired   int         `json:"cardExpired"`
}

func (a *API) Check(ctx context.Context) (APIResCheck, error) {
	url := "https://p-club.dsgapps.dk/api/cp/check"
	res, err := a.httpGET(ctx, url)
	if err != nil {
		return APIResCheck{}, err
	}

	var check APIResCheck
	if err := json.Unmarshal(res, &check); err != nil {
		return APIResCheck{}, err
	}

	return check, nil
}
