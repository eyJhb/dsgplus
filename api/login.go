package api

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

var (
	reExtractAPIKey = regexp.MustCompile("apikey=([^\"]+)")
)

type APIResSocialize struct {
	GCID string `json:"gcid"`
	UCID string `json:"ucid"`
}

type APIResUserInfo struct {
	FirstName string `json:"firstName"`
	Nickname  string `json:"nickname"`
}

type APIResAccountsGigya struct {
	UID                string         `json:"UID"`
	UIDSignature       string         `json:"UIDSignature"`
	signatureTimestamp string         `json:"signatureTimestamp"`
	UserInfo           APIResUserInfo `json:"userInfo"`
}

// API data struct
type APIReqLoginDataProfile struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Nickname  string `json:"nickname"`
	PhotoUrl  string `json:"photoUrl"`
}

type APIReqLoginData struct {
	GigyaUid                string                 `json:"gigyaUid"`
	GigyaUidSignature       string                 `json:"gigyaUidSignature"`
	GigyaEmail              string                 `json:"gigyaEmail"`
	GigyaSignatureTimestamp string                 `json:"gigyaSignatureTimestamp"`
	Profile                 APIReqLoginDataProfile `json:"profile"`
}

type APIReqLogin struct {
	ProviderName string          `json:"providerName"`
	Data         APIReqLoginData `json:"data"`
}

func (a *API) Login(ctx context.Context) error {
	if a.Email == nil || a.Password == nil || a.Shop == nil {
		return errors.New("You can only use this function, if you use email+password+shop to identify yourself")
	}

	// 1st request!
	rUrl, _ := url.Parse("https://p-idp.dsgapps.dk/apps")
	values := url.Values{
		"clientId":      {"customer-program"},
		"emailOrPhone":  {*a.Email},
		"tenantId":      {strconv.Itoa(int(*a.Shop))},
		"channel":       {"CustomerProgram"},
		"clientFlow":    {"gigya"},
		"clientTraceId": {a.clientTraceId},
		"nonce":         {mustNounce(10)},
	}
	rUrl.RawQuery = values.Encode()

	req, err := http.NewRequestWithContext(ctx, "GET", rUrl.String(), nil)
	if err != nil {
		return err
	}

	res, err := a.client.Do(req)
	if err != nil {
		return err
	}
	body := getBody(res)

	if res.StatusCode != 200 || !strings.Contains(string(body), *a.Email) {
		return errors.New("Could not make inital request")
	}

	// extract information
	apiKey := string(reExtractAPIKey.FindSubmatch(body)[1])
	pageUrl := res.Request.URL.String()

	// 2nd request!
	rUrl, _ = url.Parse("https://socialize.eu1.gigya.com/socialize.getIDs")
	values = url.Values{
		"APIKey":        {apiKey},
		"includeTicket": {"true"},
		"pageURL":       {pageUrl},
		"sdk":           {"js_latest"},
		"format":        {"json"},
	}
	rUrl.RawQuery = values.Encode()

	req, err = http.NewRequestWithContext(ctx, "GET", rUrl.String(), nil)
	if err != nil {
		return err
	}

	res, err = a.client.Do(req)
	if err != nil {
		return err
	}
	body = getBody(res)

	if res.StatusCode != 200 || !strings.Contains(string(body), "gcid") {
		return errors.New("Could not make 2nd request")
	}

	var socializeRes APIResSocialize
	if err := json.Unmarshal([]byte(body), &socializeRes); err != nil {
		return err
	}

	// 3rd request!
	rUrl, _ = url.Parse("https://accounts.eu1.gigya.com/accounts.login")
	data := url.Values{
		"loginID":           {*a.Email},
		"password":          {*a.Password},
		"sessionExpiration": {"0"},
		"targetEnv":         {"jssdk"},
		"include":           {"profile%2Cdata%2Cemails%2Csubscriptions%2Cpreferences%2C"},
		"includeUserInfo":   {"true"},
		"loginMode":         {"standard"},
		"lang":              {"da"},
		"APIKey":            {apiKey},
		"source":            {"showScreenSet"},
		"sdk":               {"js_latest"},
		"authMode":          {"cookie"},
		"pageURL":           {pageUrl},
		"gmid":              {socializeRes.GCID},
		"ucid":              {socializeRes.UCID},
		"format":            {"json"},
	}

	req, err = http.NewRequestWithContext(ctx, "POST", rUrl.String(), strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, err = a.client.Do(req)
	if err != nil {
		return err
	}
	body = getBody(res)

	if res.StatusCode != 200 || !strings.Contains(string(body), *a.Email) {
		return errors.New("Could not make 3rd request")
	}

	var accountsGigyaRes APIResAccountsGigya
	if err := json.Unmarshal([]byte(body), &accountsGigyaRes); err != nil {
		return err
	}

	// 4th request
	rUrl, _ = url.Parse(pageUrl + "/login")
	jsonDataStruct := APIReqLogin{
		ProviderName: "gigya",
		Data: APIReqLoginData{
			GigyaUid:                accountsGigyaRes.UID,
			GigyaUidSignature:       accountsGigyaRes.UIDSignature,
			GigyaEmail:              *a.Email,
			GigyaSignatureTimestamp: accountsGigyaRes.signatureTimestamp,
			Profile: APIReqLoginDataProfile{
				FirstName: accountsGigyaRes.UserInfo.FirstName,
				LastName:  "",
				Nickname:  accountsGigyaRes.UserInfo.Nickname,
				PhotoUrl:  "",
			},
		},
	}
	jsonData, err := json.Marshal([]APIReqLogin{jsonDataStruct})
	if err != nil {
		return err
	}

	data = url.Values{
		"identities": {string(jsonData)},
	}

	req, err = http.NewRequestWithContext(ctx, "POST", rUrl.String(), strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, err = a.client.Do(req)
	if err != nil {
		return err
	}
	body = getBody(res)

	if res.StatusCode != 302 || !strings.Contains(string(body), "eyJhb") {
		return errors.New("Could not make 4th request")
	}

	redirect := res.Header.Get("Location")
	token := redirect[len("http://localhost#id_token="):]
	a.Token = &token

	return nil
}
