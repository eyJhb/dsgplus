package api

import (
	"context"
	"encoding/json"
	"fmt"
)

// punchcards
type APIResPunchCard struct {
	ID                      string          `json:"id"`
	Title                   string          `json:"title"`
	ImageURL                string          `json:"imageUrl"`
	EndDate                 int             `json:"endDate"`
	ExpiryTxt               string          `json:"expiryTxt"`
	WeightTxt               string          `json:"weightTxt"`
	PunchesCollected        int             `json:"punchesCollected"`
	PunchesRequired         int             `json:"punchesRequired"`
	Activated               bool            `json:"activated"`
	PunchesCollectionStatus bool            `json:"punchesCollectionStatus"`
	StatusLabelTxt          string          `json:"statusLabelTxt"`
	PresentTitleTxt         string          `json:"presentTitleTxt"`
	PunchcardExpiryTxt      string          `json:"punchcardExpiryTxt"`
	PunchCardID             string          `json:"punchCardId"`
	Variants                []APIResVariant `json:"variants,omitempty"`
}

type APIResVariant struct {
	ID                   string   `json:"id"`
	Title                string   `json:"title"`
	Description          string   `json:"description"`
	ImageURL             string   `json:"imageUrl"`
	AdditionalImageLinks []string `json:"additionalImageLinks"`
	OfferLink            string   `json:"offerLink"`
	OfferImage           string   `json:"offerImage"`
	Price                float64  `json:"price"`
	OfferPrice           float64  `json:"offerPrice"`
	OriginalPrice        float64  `json:"originalPrice"`
	Discount             float64  `json:"discount"`
	PercentDiscount      float64  `json:"percentDiscount"`
	UnitPriceTxt         string   `json:"unitPriceTxt"`
	ExpiryTxt            string   `json:"expiryTxt"`
	PriceTxt             string   `json:"priceTxt"`
	DiscountTxt          string   `json:"discountTxt"`
	DiscountType         string   `json:"discountType"`
	DiscountValueTxt     string   `json:"discountValueTxt"`
	PriceLabelTxt        string   `json:"priceLabelTxt"`
	WeightTxt            string   `json:"weightTxt"`
	// QualityStamps        []interface{} `json:"qualityStamps"`
}

// membershipOffers
type APIResMembershipOffer struct {
	ID                         string          `json:"id"`
	Title                      string          `json:"title"`
	Description                string          `json:"description"`
	OfferLink                  string          `json:"offerLink"`
	Price                      float64         `json:"price"`
	OriginalPrice              float64         `json:"originalPrice"`
	Discount                   float64         `json:"discount"`
	PercentDiscount            float64         `json:"percentDiscount"`
	EndTime                    string          `json:"endTime"`
	SpecialOffer               string          `json:"specialOffer"`
	HidePrice                  bool            `json:"hidePrice"`
	HideSaving                 bool            `json:"hideSaving"`
	OfferImage                 string          `json:"offerImage"`
	SavingsText                string          `json:"savingsText"`
	ImageURL                   string          `json:"imageUrl"`
	AdditionalImageLinks       []string        `json:"additionalImageLinks"`
	WeightTxt                  string          `json:"weightTxt"`
	OfferType                  string          `json:"offerType"`
	OfferLimitationExceededTxt string          `json:"offerLimitationExceededTxt"`
	ExpiryTxt                  string          `json:"expiryTxt"`
	PriceTxt                   string          `json:"priceTxt"`
	DiscountTxt                string          `json:"discountTxt"`
	DiscountType               string          `json:"discountType"`
	DiscountValueTxt           string          `json:"discountValueTxt"`
	PriceLabelTxt              string          `json:"priceLabelTxt"`
	UnitPriceText              string          `json:"unitPriceText,omitempty"`
	Variants                   []APIResVariant `json:"variants,omitempty"`
	MinimumQuantity            int             `json:"minimumQuantity,omitempty"`
	OfferLimitationTxt         string          `json:"offerLimitationTxt,omitempty"`
	TopLeftBadgeTxt            string          `json:"topLeftBadgeTxt,omitempty"`
	// QualityStamps              []interface{} `json:"qualityStamps"`
}

// api res personalizedOffer
type APIResPersonalizedOffer struct {
	ID             string          `json:"id"`
	Title          string          `json:"title"`
	ImageURL       string          `json:"imageUrl"`
	EndDate        int             `json:"endDate"`
	ExpiryTxt      string          `json:"expiryTxt"`
	Discount       float64         `json:"discount"`
	DiscountType   string          `json:"discountType"`
	DiscountTxt    string          `json:"discountTxt"`
	QuantityTxt    string          `json:"quantityTxt"`
	Activated      bool            `json:"activated"`
	PriceLabelTxt  string          `json:"priceLabelTxt"`
	ClipEndpoint   string          `json:"clipEndpoint"`
	VariantsHeader string          `json:"variantsHeader,omitempty"`
	Variants       []APIResVariant `json:"variants,omitempty"`
	TermsTxt       string          `json:"termsTxt"`
	// QualityStamps  []interface{}   `json:"qualityStamps"`
}

type APIResPersonalizedOfferActivate struct {
	OfferId string
	status  string
}

func (a *API) PersonalizedOffers(ctx context.Context) ([]APIResPersonalizedOffer, error) {
	url := "https://p-club.dsgapps.dk/api/cp/personalizedOffer"
	res, err := a.httpGET(ctx, url)
	if err != nil {
		return nil, err
	}

	var offers []APIResPersonalizedOffer
	if err := json.Unmarshal(res, &offers); err != nil {
		return nil, err
	}

	return offers, nil
}

func (a *API) PersonalizedOfferActivate(ctx context.Context, id string) error {
	url := fmt.Sprintf("https://p-club.dsgapps.dk/api/cp/personalizedOffer/%s/_activate", id)
	res, err := a.httpPOST(ctx, url, nil)
	if err != nil {
		return err
	}

	var activateResponse APIResPersonalizedOfferActivate
	if err := json.Unmarshal(res, &activateResponse); err != nil {
		return err
	}

	return nil
}

func (a *API) MembershipOffers(ctx context.Context) ([]APIResMembershipOffer, error) {
	url := "https://p-club.dsgapps.dk/api/cp/membershipOffers"
	res, err := a.httpGET(ctx, url)
	if err != nil {
		return nil, err
	}

	var offers []APIResMembershipOffer
	if err := json.Unmarshal(res, &offers); err != nil {
		return nil, err
	}

	return offers, nil
}

func (a *API) PunchCards(ctx context.Context) ([]APIResPunchCard, error) {
	url := "https://p-club.dsgapps.dk/api/cp/punchCards"
	res, err := a.httpGET(ctx, url)
	if err != nil {
		return nil, err
	}

	var offers []APIResPunchCard
	if err := json.Unmarshal(res, &offers); err != nil {
		return nil, err
	}

	return offers, nil
}
