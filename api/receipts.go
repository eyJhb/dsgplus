package api

import (
	"context"
	"encoding/json"
	"net/url"
)

type Receipt struct {
	ID                     string  `json:"id"`
	MembershipID           int64   `json:"membershipId"`
	StoreID                int     `json:"storeId"`
	PosNumber              int     `json:"posNumber"`
	TransactionNumber      int     `json:"transactionNumber"`
	PostingDate            int     `json:"postingDate"`
	SalesTotal             float64 `json:"salesTotal"`
	TransactionCurrency    string  `json:"transactionCurrency"`
	MemberDiscount         float64 `json:"memberDiscount"`
	Vat                    int     `json:"vat"`
	SpendAndSaveVoucher    int     `json:"spendAndSaveVoucher"`
	DigitalRefund          int     `json:"digitalRefund"`
	MemberDiscountTxt      string  `json:"memberDiscountTxt"`
	Title                  string  `json:"title"`
	SalesTotalTxt          string  `json:"salesTotalTxt"`
	VatTxt                 string  `json:"vatTxt"`
	SpendAndSaveVoucherTxt string  `json:"spendAndSaveVoucherTxt"`
	DigitalRefundTxt       string  `json:"digitalRefundTxt"`
}

type ReceiptsList struct {
	GroupTitle      string    `json:"groupTitle"`
	GroupSavingsTxt string    `json:"groupSavingsTxt"`
	Receipts        []Receipt `json:"receipts"`
}

type APIResReceipts struct {
	ReceiptsList []ReceiptsList `json:"receiptsList"`
	// RefundNotifications []interface{} `json:"refundNotifications"`
}

type ReceiptDetailLineItem struct {
	SequenceNumber          int     `json:"sequenceNumber"`
	ArticleNumber           int64   `json:"articleNumber"`
	ArticleDescription      string  `json:"articleDescription"`
	SalesUnit               string  `json:"salesUnit"`
	QtyInSalesUnit          int     `json:"qtyInSalesUnit"`
	QtyInBaseUnit           int     `json:"qtyInBaseUnit"`
	SalesPrice              float64 `json:"salesPrice"`
	NormalPrice             float64 `json:"normalPrice"`
	Discount                float64 `json:"discount"`
	ItemType                string  `json:"itemType"`
	RefundQualifier         bool    `json:"refundQualifier"`
	Ean                     int64   `json:"ean"`
	TaxAmount               float64 `json:"taxAmount"`
	DiscountMember          int     `json:"discountMember"`
	DiscountOkoPlus         int     `json:"discountOkoPlus"`
	DiscountOther           float64 `json:"discountOther"`
	DiscountMemberTxt       string  `json:"discountMemberTxt"`
	DiscountOkoPlusPriceTxt string  `json:"discountOkoPlusPriceTxt"`
	DiscountOtherPriceTxt   string  `json:"discountOtherPriceTxt"`
	DiscountTypeTxt         string  `json:"discountTypeTxt"`
	DiscountDescription     string  `json:"discountDescription"`
	DiscountPriceTxt        string  `json:"discountPriceTxt"`
	NormalPriceTxt          string  `json:"normalPriceTxt"`
}

type ReceiptDetailAddress struct {
	Brand  string `json:"brand"`
	Street string `json:"street"`
	City   string `json:"city"`
}

type APIResReceiptDetail struct {
	Header                string                  `json:"header"`
	SubHeader             string                  `json:"subHeader"`
	RefundAllowed         bool                    `json:"refundAllowed"`
	Barcode               string                  `json:"barcode"`
	BarcodeTxt            string                  `json:"barcodeTxt"`
	ID                    string                  `json:"id"`
	StoreID               int                     `json:"storeId"`
	PosNumber             int                     `json:"posNumber"`
	TransactionNumber     int                     `json:"transactionNumber"`
	PostingDate           int                     `json:"postingDate"`
	LineItems             []ReceiptDetailLineItem `json:"lineItems"`
	Address               ReceiptDetailAddress    `json:"address"`
	DiscountSumMember     int                     `json:"discountSumMember"`
	DiscountSumMemberTxt  string                  `json:"discountSumMemberTxt"`
	DiscountSumOkoPlus    int                     `json:"discountSumOkoPlus"`
	DiscountSumOkoPlusTxt string                  `json:"discountSumOkoPlusTxt"`
	DiscountSumOther      float64                 `json:"discountSumOther"`
	DiscountSumOtherTxt   string                  `json:"discountSumOtherTxt"`
	DiscountTotal         float64                 `json:"discountTotal"`
	DiscountTotalTxt      string                  `json:"discountTotalTxt"`
}

func (a *API) Receipts(ctx context.Context) (APIResReceipts, error) {
	url := "https://p-club.dsgapps.dk/api/cp/receipt"
	res, err := a.httpGET(ctx, url)
	if err != nil {
		return APIResReceipts{}, err
	}

	var receipts APIResReceipts
	if err := json.Unmarshal(res, &receipts); err != nil {
		return APIResReceipts{}, err
	}

	return receipts, nil
}

func (a *API) Receipt(ctx context.Context, id string) ([]APIResReceiptDetail, error) {
	rUrl, _ := url.Parse("https://p-club.dsgapps.dk/api/cp/receipt/details")
	rUrl.RawQuery = url.Values{"receiptId": {id}}.Encode()
	res, err := a.httpGET(ctx, rUrl.String())
	if err != nil {
		return nil, err
	}

	var receipt []APIResReceiptDetail
	if err := json.Unmarshal(res, &receipt); err != nil {
		return nil, err
	}

	return receipt, nil
}
