package api

import (
	"errors"
	"net/http"
	"net/http/cookiejar"
)

type Shop uint8

var (
	NETTO  Shop = 4
	FOETEX Shop = 3
	BILKA  Shop = 2

	ShopToString = map[Shop]string{
		NETTO:  "Netto",
		FOETEX: "Føtex",
		BILKA:  "Bilka",
	}
)

type API struct {
	client        *http.Client
	clientTraceId string

	Shop     *Shop
	Email    *string
	Password *string
	Token    *string
}

func New(api *API) (*API, error) {
	jar, err := cookiejar.New(nil)
	if err != nil {
		return nil, err
	}
	api.client = &http.Client{
		CheckRedirect: customRedirect,
		Jar:           jar,
	}
	api.clientTraceId = mustNounce(10)

	if (api.Email == nil || api.Password == nil || api.Shop == nil) && api.Token == nil {
		return nil, errors.New("You must specify either email+password+shop or a token")
	}

	return api, nil
}

func (a *API) ensureInitialised() error {
	if a.Token == nil {
		return errors.New("ensureInitialised: we don't have any token, thereby not initalised")
	}
	return nil
}

func customRedirect(req *http.Request, via []*http.Request) error {
	// log.Printf("customRedirect value '%s'", req.URL.String())
	if req.URL.Host == "localhost" {
		return http.ErrUseLastResponse
	}
	return nil
}
