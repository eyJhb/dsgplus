# file generated from go.mod using vgo2nix (https://github.com/nix-community/vgo2nix)
[
  {
    goPackagePath = "github.com/go-chi/chi";
    fetch = {
      type = "git";
      url = "https://github.com/go-chi/chi";
      rev = "v1.5.1";
      sha256 = "1xfaq1wfa8p11cbrz3naz9ms4x0n2yni4v0halaqbc5rnn2srp2k";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/patrickmn/go-cache";
    fetch = {
      type = "git";
      url = "https://github.com/patrickmn/go-cache";
      rev = "v2.1.0";
      sha256 = "10020inkzrm931r4bixf8wqr9n39wcrb78vfyxmbvjavvw4zybgs";
      moduleDir = "";
    };
  }
]
