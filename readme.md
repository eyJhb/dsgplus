# Salling Group Plus apps!
After Salling Group has announced their plus apps, I got fed up with having to have one app for each store (Foetex, Netto and Bilka) that each takes up 65 MB.

Therefore I made this integration by inspecting the API calls made from the Android app and made it available using a web interface.

The website does not save any information about you on the server, it will store things in cache for 30 minutes, and only use your credentials to obtain a token.
This token will afterwards be placed as a cookie in your own browser, and will not be stored on the server.


Please enjoy! 

# Current work
I am currently trying to get this somewhat stable, and get a server up and running with this.
