package main

import (
	"flag"

	"gitlab.com/eyJhb/dsg-plus/web"
)

var (
	flagWebPort = flag.String("port", "8080", "The port to run the server on")
	flagWebBind = flag.String("bind", "0.0.0.0", "Which interface to bind on")
)

func main() {
	// parse our flags
	flag.Parse()

	w := &web.Web{ServerPort: *flagWebPort, ServerBind: *flagWebBind}
	w = web.New(w)
	w.Start()
}
