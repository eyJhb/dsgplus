module gitlab.com/eyJhb/dsg-plus

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
