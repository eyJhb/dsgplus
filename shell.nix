{ pkgs ? import <nixpkgs> {} }:

let
in pkgs.mkShell {
  buildInputs = [
    (import ./build.nix {})
  ];
}
