{ pkgs ? import <nixpkgs> {} }:
# { buildGoPackage, fetchFromGitHub }:

let
  bin = pkgs.buildGoPackage rec {
    name = "dsg-plus";
    version = "v0.0.1";
    goPackagePath = "gitlab.com/eyJhb/dsg-plus";
    src = ./.;
    goDeps = ./deps.nix;
  };
in pkgs.stdenv.mkDerivation rec {
  name = "dsg-plus";
  src = ./.;
  installPhase = ''
    mkdir $out
    cp ${bin}/bin/dsg-plus $out
    cp -R ${src}/web $out
  '';
}
